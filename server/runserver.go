package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"github.com/jwl/nimbasa-grand-central/internal"
)

func getAllPokemon(w http.ResponseWriter, r *http.Request) {
	pokemonData := internal.GetPokemonFromFile()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(pokemonData)
}

func getPokemon(w http.ResponseWriter, r *http.Request) {
	pokemonData := internal.GetPokemonFromFile()
	w.Header().Set("Content-Type", "application/json")

	targetNumber, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		fmt.Println("id passed to pokemon/{id} is not an int")
	}

	for _, singlePokemon := range pokemonData {
		if singlePokemon.Number == targetNumber {
			log.Printf("Returning pokemon with number <%d>:<%s>", targetNumber, singlePokemon.Name)
			json.NewEncoder(w).Encode(singlePokemon)
			fmt.Printf("old method: %#v", singlePokemon)
			fmt.Printf("new method: %#v", internal.GetSinglePokemon(targetNumber))
		}
	}

}

func updatePokemon(w http.ResponseWriter, r *http.Request) {
	// implement later
}

func deletePokemon(w http.ResponseWriter, r *http.Request) {
	// implement later
}

func main() {
	router := mux.NewRouter()

	// endpionts
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "API response!\nNow on multiple lines.")
	})
	router.HandleFunc("/pokemon", getAllPokemon).Methods("GET")
	router.HandleFunc("/pokemon/{id}", getPokemon).Methods("GET")
	router.HandleFunc("/pokemon/{id}", updatePokemon).Methods("POST")
	router.HandleFunc("/pokemon/{id}", deletePokemon).Methods("DELETE")

	fmt.Println("Server listening!")
	log.Fatal(http.ListenAndServe(":5000", router))
}
