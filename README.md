# nimbasa-grand-central
Golang implementation of a Pokemon battle engine as a backend API. Built off of template project [go-docker-tutorial](https://github.com/jwl/go-docker-tutorial)

### Setup
* From project root, run: `docker build -t nimbasa .`

### Usage
* run the following command: `docker-compose up nimbasa`
* server should now be listening on port 5000 on `localhost`

### References
* https://medium.com/better-programming/building-a-restful-api-with-go-and-mongodb-93e59cbbee88
