package main

import (
	"fmt"

	"github.com/jwl/nimbasa-grand-central/internal"
)

func main() {
	fmt.Println("Hello world")
	// pokemonData := internal.GetPokemonFromFile("./pokemondata/stats.json")
	pokemonData := internal.GetPokemonFromFile()
	for key, pokemon := range pokemonData {
		fmt.Println("Reading Value for Key :", key)
		fmt.Printf("\tnumber: %d, name: %s\n", pokemon.Number, pokemon.Name)
	}

}
